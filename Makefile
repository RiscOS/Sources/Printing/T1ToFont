# Copyright 1996 Acorn Computers Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# Makefile for T1ToFont 
#

COMPONENT  = T1ToFont
override TARGET = !RunImage
INSTTYPE   = app
OBJS       = frontend convert encoding metrics type1 utils
CDEFINES   = -DTRUSTFONTBBOX=0
CINCLUDES  = ${RINC}
LIBS      += ${RLIB}
INSTAPP_FILES = !Help !Run !Boot !RunImage Messages Templates \
                !Sprites:Themes !Sprites11:Themes !Sprites22:Themes \
                Morris4.!Sprites:Themes.Morris4 Morris4.!Sprites22:Themes.Morris4 \
                Ursula.!Sprites:Themes.Ursula Ursula.!Sprites22:Themes.Ursula \
                Encodings.Greek:Encodings    Encodings.Selwyn:Encodings \
                Encodings.Sidney:Encodings   Encodings.Sussex:Encodings \
                Encodings.SysFixed:Encodings Encodings.SysFixed4:Encodings \
                Encodings.System:Encodings   Encodings.TeX:Encodings \
                Encodings.Specials.Accents_Dn:Encodings.Specials \
                Encodings.Specials.Accents_Up:Encodings.Specials \
                Encodings.Specials.Adobe:Encodings.Specials \
                Encodings.Specials.Dummies:Encodings.Specials
INSTAPP_VERSION = Messages

include CApp

# Dynamic dependencies:
