/* Copyright 1996 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
#include "kernel.h"

#include "main.h"
#include "encoding.h"
#include "metrics.h"
#include "type1.h"
#include "rescale.h"
#include "adb.h"
#include "outlines.h"
#include "remap.h"


/*---- exported functions ---- */

os_error *settype(const char *fname, int type)
{
    char buffer[256];

    sprintf(buffer, "settype %s &%x", fname, type);
    return (os_cli(buffer));
}


void errorexit(os_error * e)
{
     _kernel_raise_error((_kernel_oserror *) e);
}


void erfn(int result)
{
    if (result == EOF)
	 errorexit((os_error *) _kernel_last_oserror());
}

int fput3(int x, int y, FILE * outfile)
{
    if (fputc((x >> 0) & 0xFF, outfile) == EOF)
	 return (EOF);
    if (fputc(((x >> 8) & 0x0F) | ((y << 4) & 0xF0), outfile) == EOF)
	return (EOF);
    return (fputc((y >> 4) & 0xFF, outfile));
}


int fput4(int x, FILE * outfile)
{
    if (fputc((x >> 0) & 0xFF, outfile) == EOF)
	 return (EOF);
    if (fputc((x >> 8) & 0xFF, outfile) == EOF)
	return (EOF);
    if (fputc((x >> 16) & 0xFF, outfile) == EOF)
	return (EOF);
    return (fputc((x >> 24) & 0xFF, outfile));
}
